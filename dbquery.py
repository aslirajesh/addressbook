from sqlalchemy.orm import Session
import models
import schemas


class Address:
    """
    db operations for Address model
    """
    def get_address(self: Session):
        return self.query(models.Address).all()

    def get_address_by_id(self: Session, address_id):
        return self.query(models.Address).filter_by(id=address_id).first()

    def create_address(self: Session, request: schemas.AddressCreate):
        db_address = models.Address(**request.dict())
        self.add(db_address)
        self.commit()
        return db_address

    def fetch_by_id(self: Session, _id):
        return self.query(models.Address).\
            filter(models.Address.id == _id).first()

    def update(self: Session, item_data):
        updated_item = self.merge(item_data)
        self.commit()
        return updated_item

    def delete(self: Session, item_id):
        db_item = self.query(models.Address).filter_by(id=item_id).first()
        self.delete(db_item)
        self.commit()
