### Task:
Create an address book application where API users can create, update and delete
addresses.
The address should:
- contain the coordinates of the address.
- be saved to an SQLite database.
- be validated

API Users should also be able to retrieve the addresses that are within a given distance and
location coordinates.

### Requirements:
 You should have python 3.8 or higher installed and also pip3 installed in your system.
## How to run the application

clone the repository to your local machine
```bash
$ git clone git@gitlab.com:aslirajesh/addressbook.git
```

Install dependencies from requirements.txt
```bash
$ cd AddressBook
$ pip install -r requirements.txt
```

Run the application
```bash
$ uvicorn main:app --reload
```

## API
for the API you can use the following endpoints:

 - GET /api/addresses - returns all addresses
 - GET /api/addresses/{id} - returns an address with the given id
 - POST /api/addresses - creates a new address
 - PUT /api/addresses/{id} - updates an address with the given id
 - DELETE /api/addresses/{id} - deletes an address with the given id
 - GET /api/addresses/address-within-radius - returns all addresses that are 
   within a given distance and location coordinates
   
## Documentation
You can find the documentation for the API at the following link:
your-machine-address:8000/api/docs

   most probably at : localhost:8000/docs

