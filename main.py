from typing import List, Dict
from fastapi import Depends, FastAPI, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
import dbquery
import geo_location
import models
import schemas
from database import engine, get_db

models.Base.metadata.create_all(bind=engine)
app = FastAPI()


@app.get("/api/address/", response_model=Dict[str, List[schemas.AddressView]])
def read_address(db: Session = Depends(get_db)):
    """
    Return all stored addresses
    """
    address = dbquery.Address.get_address(db)
    return {"data": address}


@app.get("/api/address/{address_id}", response_model=schemas.AddressView)
def read_address(address_id: int, db: Session = Depends(get_db)):
    """
    Return an address by id
    """
    address = dbquery.Address.get_address_by_id(db, address_id)
    return {"data": address}


@app.post("/api/address/", response_model=schemas.AddressView, status_code=201)
def create_address(
        request: schemas.AddressCreate, db: Session = Depends(get_db)):
    """
    Store new address
    """
    return dbquery.Address.create_address(self=db, request=request)


@app.put("/api/address/{address_id}", response_model=schemas.AddressView)
def update_address(
        address_id: int, request: schemas.AddressCreate,
        db: Session = Depends(get_db)):
    """
    Update an existing address
    """
    db_item = dbquery.Address.fetch_by_id(db, address_id)
    if db_item:
        update_item_encoded = jsonable_encoder(request)
        db_item.longitude = update_item_encoded['longitude']
        db_item.latitude = update_item_encoded['latitude']

        return dbquery.Address.update(self=db, item_data=db_item)
    else:
        raise HTTPException(status_code=400,
                            detail="Wrong id")


@app.delete('/api/address/{address_id}')
def delete_address(address_id: int, db: Session = Depends(get_db)):
    """
    Delete an existing address with the given ID
    """
    db_item = dbquery.Address.fetch_by_id(db, address_id)
    if db_item is None:
        raise HTTPException(status_code=404,
                            detail="Wrong id")
    dbquery.Address.delete(db, address_id)
    return {"Address deleted!"}


@app.get("/api/address/address-within-radius/",
         response_model=Dict[str, List[schemas.AddressView]])
def get_address_within_radius(
        latitude: float, longitude: float, radius_in_km: float, db: Session =
        Depends(get_db)):
    """
    Return all addresses within the given radius
    """
    address = geo_location.address_within_radius(
        latitude, longitude, radius_in_km, db)

    return {"data": address}
