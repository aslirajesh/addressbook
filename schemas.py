from pydantic import BaseModel


class AddressView(BaseModel):
    """
        Added response schema for Address model
    """
    id: int
    latitude: float
    longitude: float

    class Config:
        orm_mode = True


class AddressCreate(BaseModel):
    """
        Added request schema for create Address
    """
    latitude: float
    longitude: float
