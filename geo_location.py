from geopy import distance
import dbquery


def calculate_deo_distance(center_point_tuple, test_point_tuple, radius):
    """
    Calculate the distance between two points.
    """
    dis = distance.great_circle(center_point_tuple, test_point_tuple).km
    if dis < float(radius):
        return True
    else:
        return False


def address_within_radius(latitude, longitude, radius_in_km, db):
    """
    Return all addresses within the given radius
    """
    all_addresses = dbquery.Address.get_address(db)
    location_in_circle = []
    for address in all_addresses:
        if calculate_deo_distance((latitude, longitude),
                                  (address.latitude, address.longitude),
                                  radius_in_km):
            location_in_circle.append(address)
    return location_in_circle
