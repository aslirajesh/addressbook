from sqlalchemy import Column, Integer, String
from database import Base


class Address(Base):
    """
    Create an Address model with fields
    """
    __tablename__ = "address_book"

    id = Column(Integer, primary_key=True, index=True)
    latitude = Column(String(50), nullable=False)
    longitude = Column(String(50), nullable=False)
